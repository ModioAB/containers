#! /bin/bash
#
# Handle binfmt_misc registrations for qemu-user emulation
#
# Assumes a Fedora + systemd filesystem layout
#
set -euo pipefail
trap exit HUP INT QUIT TERM

binfmt_mnt=/proc/sys/fs/binfmt_misc

die() {
    # shellcheck disable=SC2319
    local err=${1:-$?}
    cat >&2
    exit $((err))
}

binfmt() {
    /usr/lib/systemd/systemd-binfmt "$@" \
    || die <<ERROR
FATAL: systemd-binfmt $@
ERROR
}

# Pick the config file names from discovered configs
binfmt_configs() {
    binfmt --cat-config | sed '/#/s|.*/||p;d'
}

usage() {
    cat <<USAGE
usage: ${0} [--enabled|--disabled] [--replace] [--] [systemd-binfmt args ...]
    --disabled      Sets binfmt_misc status to disabled
    --enabled       Sets binfmt_misc status to enabled
    --pause         Pause forever when finished, instead of exiting
    --replace       Unregister all existing entries before registering new ones

$(binfmt --help)

Available configs:
$(binfmt_configs)
USAGE
    exit
}

# Exit early when only requesting help
[[ "$*" == --help ]] && usage

# Verify that kernel module is loaded
if [[ ! -d $binfmt_mnt ]]; then
    die 99 <<ERROR
FATAL: ${binfmt_mnt} does not exist
       is kernel module not loaded?

$(lsmod)
ERROR
fi

# Ensure that binfmt_misc file system is mounted
if [[ ! -d $binfmt_mnt/register ]]; then
    mount -t binfmt_misc binfmt_misc $binfmt_mnt \
    || die <<ERROR
FATAL: could not mount binfmt_misc file system
ERROR
fi

pause=false

while [[ $# -gt 0 ]]; do
    case "$1" in
    --help )
        usage
    ;;
    --disabled )
        echo 0 > $binfmt_mnt/status
    ;;
    --enabled )
        echo 1 > $binfmt_mnt/status
    ;;
    --pause )
        pause=true
    ;;
    --replace )
        binfmt --unregister
    ;;
    -- )
        shift
        break
    ;;
    * )
        break
    esac
    shift
done

printf 'binfmt: %s\n' "$(cat $binfmt_mnt/status)"

binfmt "$@"

while $pause; do sleep 3600; done
